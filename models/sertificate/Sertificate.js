var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
  var Patient = require('../users/Patient.js').Patient;

var schema = new Schema({
    patient: {
      type: Schema.Types.ObjectId,
      ref: "Patient"
    },
    name: {
      type: String
    },
    code: {
        type: String
    },
    validity: {
        type: Date,
        default: Date.now
    },
    passed: {
      type: Number,
      default: 0
    },
    quantity: {
      type: Number
    },
    order: {
        type: Boolean,
        default: false
    },
    medicalData: [{
        name: {
            type: String
        },
        result: {
            type: Boolean,
            default: false
        },
        status: {
          type: Boolean,
          default: 0
        }
    }]
});

schema.statics.createNew = async function(_idPatient) {
    var Self = this
    console.log(_idPatient);
    let patient = await Patient.findOne({_id:_idPatient});
    console.log(patient);
    let sertificateConst = [
      {
        patient: _idPatient,
        name: 'Справка для занятий физической культурой',
        code: 'У-046',
        quantity: 2,
        medicalData: [
           {
              name: 'Флюрография',
           },
           {
              name: 'ЭКГ'
           }
        ]
      },
      {
        patient: _idPatient,
        name: 'Справка для ГТО',
        code: 'Р-070',
        quantity: 2,
        medicalData: [
           {
              name: 'ЭКГ'
           },
           {
              name: 'осмотр терапевта'
           }
        ]
      },
      {
        patient: _idPatient,
        name: 'Справка для Поселения в общежитие',
        code: 'У-075',
        quantity: 2,
        medicalData: [
           {
             name: 'Флюрография',
           },
           {
             name: 'осмотр дерматолога'
           }
        ]
      }
    ]
    if(patient) {
        sertificateConst.forEach((sertificate) => {
            let newSertificate = new this(sertificate);
            newSertificate.save();
        })
        return {data: 'ok'}
    }
    else {
      return {error: 'patient_not_exist'};
    }
};
schema.statics.updateMedicalData = async function(_idPatient) {
    var Self = this;
    let patient = await Patient.findOne({_id: _idPatient});
    let patientMedData = patient.medicalData;
    let sertificates = await this.find({patient: _idPatient});
    for (let i = 0; i < sertificates.length; ++i) {
        let sertificateMedData = sertificates[i].medicalData;
        for(let j = 0; j < sertificateMedData.length; ++j) {
            let medDataIndex = patientMedData.findIndex(el => el.name === sertificateMedData[j].name);
            console.log(medDataIndex);
            let elPatientMedData = patientMedData[medDataIndex];

            if(elPatientMedData.status === true && elPatientMedData.result === true) {
                sertificates[i].passed += 1;
            }
            sertificateMedData[j].status = elPatientMedData.status;
            sertificateMedData[j].result = elPatientMedData.result;
        }
        await sertificates[i].save();
    }
}
schema.statics.get = async function(_idPatient) {
    var Self = this;
    let sertificates = await this.find({patient:_idPatient});
    return sertificates;
}
schema.statics.order = async function(_idSertificate) {
    var Self = this;
    let sertificate = await this.findOne({_id:_idSertificate});
    console.log(sertificate);
    if(sertificate.passed == sertificate.quantity) {
      sertificate.order = true;
      sertificate = await sertificate.save();
      return sertificate;
    }
    else {
      return {error: "not_passed"};
    }
}
module.exports = Sertificate = mongoose.model('Sertificate', schema);
