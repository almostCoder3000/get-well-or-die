var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var crypto = require('crypto');
var async = require('async');
var config = require('../../config.js');
var _ = require('lodash');

var debug = require('debug')('get-well-or-die:server');

var schema = new Schema({
    login: {
        type: String,
        unique: true
    },
    hashedPassword: {
    type: String,
    required: true
    },
    salt: {
      type: String,
      required: true
    },
    created: {
      type: Date,
      default: Date.now
    },
    secondName: {
      type: String,
      default: ''
    },
    firstName: {
      type: String,
      default: ''
    },
    patronymic: {
      type: String
    },
    lastEntered: {
      type: Date,
      default: Date.now
    }
})
schema.methods.encryptPassword = function (password) {
  return crypto.createHmac('sha1', this.salt).update(config.appSalt).update(password).digest('hex');
};

schema.virtual('password')
  .set(function (password) {

    this._plainPassword = password;
    console.log('password: ', password);

    this.salt = Math.random() + '';
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function () { return this._plainPassword; });



schema.methods.checkPassword = function (password) {
  return this.encryptPassword(password) === this.hashedPassword;
};



schema.statics.authorize = function (login, password, callback) {

  var BaseUser = this;

  async.waterfall([
    function (done) {
      BaseUser.findOne({ login: login }, done);
    },
    function (user, done) {
      if (user) {
        if (user.checkPassword(password)) {

          if (user) {
            user = user.toObject();
            delete user.hashedPassword;
            delete user.salt;
          }
          console.log('USER::: ', user);

          done(null, user);
        } else {
          done("Пользователь не найден");
        }
      } else {
        done("Пользователь не найден");
      }
    }
  ], callback);
};

schema.statics.createNew = function (login, password, obj, callback) {
  var BaseUser = this;
  async.waterfall([
    function (done) {
      BaseUser.findOne({ login: login }, function (err, usr) {
        done(null, usr);
      });
    },
    function (user, done) {
      if (user) {
        done(null, "Пользователь с таким именем существует");
      } else {

        var user = new BaseUser(obj);
        user.save(function (err) {
          if (err) return callback(err);

          // console.log('user: ', user);


          if (user) {
            user = user.toObject();
            delete user.hashedPassword;
            delete user.salt;
          }

          done(null, user);
        });
      }
    }
  ], function (err, user) {
    callback(err, user);
  });
};



exports.BaseUser = BaseUser = mongoose.model('BaseUser', schema);
