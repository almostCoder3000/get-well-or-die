var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var BaseUser = require('./BaseUser.js').BaseUser;

var Doctor = BaseUser.discriminator('Doctor', new Schema ({
    role: {
        type: String,
        default: 'teacher'
    }
}, {discriminatorKey: 'kind'}));

exports.Doctor = Doctor;
