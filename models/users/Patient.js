var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var BaseUser = require('./BaseUser.js').BaseUser;

var Patient = BaseUser.discriminator('Patient', new Schema ({
    role: {
        type: String,
        default: 'patient'
    },
    group: {
        type: String
    },
    numberPolicy: {
        type: String
    },
    bloodType: {
        type: String
    },
    adress: {
        type: String
    },
    medicalData: [{
        name: {
            type: String
        },
        result: {
            type: Boolean
        },
        status: {
          type: Boolean,
          default: false
        }
    }]
}, {discriminatorKey: 'kind'}));

Patient.updatePatient = async function(data, callback) {
    let Self = this;
    let patient = await this.findOne({_id: data._idPatient});
    if(patient) {
        patient = await this.update({_id: data._idPatient}, {$set: {
            group: data.group,
            numberPolicy: data.numberPolicy,
            bloodType: data.bloodType,
            adress: data.adress
        }});
        patient = await this.findOne({_id: data._idPatient}).lean();
        delete patient.login;
        delete patient.salt;
        delete patient.hashedPassword;
        callback(null, patient);
    }
    else {
        console.log('not_patient');
        callback({error: 'patient_not_exist'}, null);
    }
};

Patient.get = async function(_idPatient, callback) {
    let Self = this;
    let patient = await this.findOne({_id: _idPatient});
    if(patient) {
        callback(null, patient);
    }
    else {
      callback(err,null);
    }
};

Patient.addMedicalData = async function(data) {
    let Self = this;
    let patient = await this.findOne({_id: data._idPatient}).lean();
    let medicalData = {
        name: data.name,
        result: data.result,
        status: data.status
    };
    if(patient) {
        patient.medicalData.push(medicalData);
        patient = await this.update({_id: data._idPatient}, {$set: {medicalData: patient.medicalData}});
        patient = await this.findOne({_id: data._idPatient});
        return patient;
    }
    else {
      return {error: 'patient_not_exist'};
    }
};

Patient.addMedicalDataConst = async function(_idPatient) {
    let Self = this;
    let patient = await this.findOne({_id:_idPatient}).lean();
    let medicalData = [
      {
        name: "ЭКГ",
        status: true,
        result: true
      },
      {
        name: "осмотр терапевта",
        status: true,
        result: true
      },
      {
        name: "Флюрография",
        status: false,
        result: false
      },
      {
        name: "Осмотр окулиста",
        status: false,
        result: true
      },
      {
        name:"осмотр дерматолога",
        status: true,
        result: false
      }
    ]
    if(patient) {
        patient = await this.update({_id: _idPatient}, {$set: {medicalData: medicalData}});
        patient = await this.findOne({_id: _idPatient});
        return patient;
    }
    else {
      return {error: 'patient_not_exist'};
    }
}

exports.Patient = Patient;
