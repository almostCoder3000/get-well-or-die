var express = require('express');
var router = express.Router();
var cors = require('cors');
var Setrificate = require('../models/sertificate/Sertificate.js');

router.get('/init', async function(req, res, next) {
    if(Object.keys(req.query).length > 0) {
        let sertificate = await Sertificate.createNew(req.query._idPatient);
        res.send(sertificate);
    }
    else {
        res.send({error: 'not_parameters'});
    }
});
router.post('/updateMedicalData', async function(req, res, next) {
  if(Object.keys(req.body).length > 0) {
      await Sertificate.updateMedicalData(req.body._idPatient);
      res.send({data: "ok"});
  }
  else {
      res.send({error: 'not_parameters'});
  }
})
router.get('/get', async function(req, res, next) {
    if(Object.keys(req.query).length > 0) {
        let sertificates = await Sertificate.get(req.query._idPatient);
        res.send(sertificates);
    }
    else {
        res.send({error: 'not_parameters'});
    }
})

router.post('/order', async function(req, res, next) {
    if(Object.keys(req.body).length > 0) {
        let sertificate = await Sertificate.order(req.body._idSertificate);
        res.send(sertificate);
    }
    else {
        res.send({error: 'not_parameters'});
    }
})
module.exports = router;
