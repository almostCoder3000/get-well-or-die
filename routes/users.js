var express = require('express');
var router = express.Router();
var passport = require('passport');
var BaseUser = require('../models/users/BaseUser.js').BaseUser;
var Doctor = require('../models/users/Doctor.js').Doctor;
var Patient = require('../models/users/Patient.js').Patient;
var async = require('async');
var debug = require('debug');
var _ = require("lodash");
var router = express.Router();
var cors = require('cors');

router.get('/init', function(req, res, next) {
    const initUsersArray = [
        {
            login: 'patient',
            password: '1',
            firstName: 'Артем',
            secondName: 'Хрушков',
            patronymic: 'Евгеньевич',
            Class: Patient
        },
        {
            login: 'doctor',
            password: '1',
            firstName: 'Максим',
            secondName: 'Рябов',
            patronymic: 'Сергеевич',
            Class: Doctor
        }
    ]
    async.each(initUsersArray, (el, next) => {
      BaseUser.findOne({ login: el.login }, (err, user) => {
        if (!user) {
          el.Class.createNew(el.login, el.password, el, (err, usr) => {
            debug('create new user: ', usr);
            next();
          });

        } else return next();
      });
    }, err => {
      debug('init user list: ', _.map(initUsersArray, 'login'));
    });
    res.json({"mess": "ok"});
});

router.post('/login', function(req, res, next) {
    console.log('body:', req.body);
    console.log('query:', req.query);

    passport.authenticate('local',
        function (err, user, info) {
            console.log('user:',user);
            return err
                ? res.json({ error: "failed error" })
                : user
                    ? req.logIn(user, function (err) {
                        return err
                            ? next(err)
                            : res.json({
                                  data: {
                                      user: user
                                  }
                              });
                    })
                    : res.json({error: "failed not user"});
        }
    )(req, res, next);
});

router.post('/logout', function(req, res, next) {
    req.logout();
    res.json({data: "success logout"});
});

module.exports = router;
